const WebSocket = require('ws')
const http = require('http')
const path = require('path')
const fs = require('fs')

const wss = new WebSocket.Server({ noServer: true });
const server = http.createServer((req, res) => {
  if (req.url === '/') {
    serveStaticFile(res, './public/index.html')
    return
  }

  // static file kiszolgálás publicból

  // https://stackabuse.com/node-http-servers-for-static-file-serving/
  let resolvedBase = path.resolve('./public');
  let safeSuffix = path.normalize(req.url).replace(/^(\.\.[\/\\])+/, '');
  let fileLoc = path.join(resolvedBase, safeSuffix);

  serveStaticFile(res, fileLoc)
})

function serveStaticFile(res, fileLoc) {
  fs.readFile(fileLoc, function(err, data) {
    if (err) {
      res.writeHead(404, 'Not Found');
      res.write('404: File Not Found!');
      return res.end();
    }

    res.statusCode = 200;

    res.write(data);
    return res.end();
  });
}

wss.on('connection', function connection(ws) {
  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
  });

  ws.send('something');
});

server.on('upgrade', function upgrade(request, socket, head) {
  wss.handleUpgrade(request, socket, head, function done(ws) {
    wss.emit('connection', ws, request, client);
  });
});

server.listen(4000, null, () => {
  console.log(`Server running at http://maze3d.127.0.0.1.nip.io:4000/`);
});
