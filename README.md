The project is based on a http + websocket server (for realtime communication). The server is nodeJS based and can be run as Docker container.

The player only needs a browser to connect and play.

# Development

docker-compose up -d

Open http://maze3d.127.0.0.1.nip.io

