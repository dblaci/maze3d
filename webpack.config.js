const path = require('path');

//const NodemonPlugin = require('nodemon-webpack-plugin'); // Ding

module.exports = {
  entry: './src/server.js',
  output: {
    path: path.resolve('./public/js'),
    filename: 'server.js',
  },
  plugins: [
    new NodemonPlugin(), // Dong
  ],
};
